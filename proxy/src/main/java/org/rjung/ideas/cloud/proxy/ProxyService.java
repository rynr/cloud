package org.rjung.ideas.cloud.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@EnableZuulProxy
@EnableEurekaClient
public class ProxyService {

    public static void main(String[] args) {
        SpringApplication.run(ProxyService.class, args);
    }
}
