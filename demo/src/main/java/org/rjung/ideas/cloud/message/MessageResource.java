package org.rjung.ideas.cloud.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
@Service("messageResource")
public class MessageResource {

    private static final Logger LOG = LoggerFactory
            .getLogger(MessageResource.class);

    @Autowired
    Environment env;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getMessage() {
        String info = getInfo();
        LOG.info("getMessage: " + info);
        return info;
    }

    private String getInfo() {
        return env.getProperty("info", "No info");
    }

}
