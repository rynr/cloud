package org.rjung.ideas.cloud.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "org.rjung.ideas.cloud.message")
@EnableEurekaClient
public class MessageService {

    public static void main(String[] args) {
        SpringApplication.run(MessageService.class, args);
    }
}
