package org.rjung.ideas.cloud.intercept;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "org.rjung.ideas.cloud.intercept")
@EnableEurekaClient
public class InterceptService {

    public static void main(String[] args) {
        SpringApplication.run(InterceptService.class, args);
    }
}
