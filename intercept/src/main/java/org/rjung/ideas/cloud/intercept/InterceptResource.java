package org.rjung.ideas.cloud.intercept;

import java.text.DateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Service("messageResource")
public class InterceptResource {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getMessage() {
        return getTime() + ": "
                + restTemplate.getForObject("http://demo/", String.class);
    }

    public String getTime() {
        return DateFormat.getDateTimeInstance().format(new Date());
    }
}
